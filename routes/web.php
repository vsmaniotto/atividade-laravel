<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('empresa', function() {
    return view('empresa');
})->name('empresa');

Route::get('servicos', function () {
    return view('servicos');
})->name('servicos');

Route::get('contato', function(){
    return view('contato');
})->name('contato');