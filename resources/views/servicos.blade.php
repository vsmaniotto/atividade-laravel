<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Serviços</title>
</head>
<body>
    <ul>
        <li><a href="{{route('home')}}">Home</a></li>
        <li><a href="{{route('empresa')}}">Empresa</a></li>
        <li><a href="{{route('servicos')}}">Serviços</a></li>
        <li><a href="{{route('contato')}}">Contato</a></li>
    </ul>

    <h1>Serviços</h1>
    <ul>
        <li>Site</li>
        <li>E-mail Marketing</li>
        <li>Landing Page</li>
        <li>Sistemas Web</li>
    </ul>
    <button>Enviar</button>
</body>
</html>